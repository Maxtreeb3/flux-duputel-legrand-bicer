import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST.test'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client

class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, email='samy@upjv', password='123'):
        return self._client.post(
            '/login',
            data={'email': email, 'password': password}
        )

    def logout(self):
        return self._client.get('/logout')


def test_auth(client):
    return AuthActions(client)