from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import PasswordInput
from feedparser import *

class SignIn(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),])
    login = StringField('Identifiant', validators=[DataRequired(),])
    password = StringField('Mot de passe', widget=PasswordInput(hide_value=True))
    pass

class LogIn(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),])
    password = StringField('Mot de passe', widget=PasswordInput(hide_value=True))
    pass

class AddFlux(FlaskForm):
    lien = StringField('Lien du flux', validators=[DataRequired()])
    pass
