LPRGI Groupe Cisco/ERP - Web Dynamique

Projet réalisé par DUPUTEL Maxence, LEGRAND Alexis et BICER Timuran


Ce projet est une application web qui permet à un utilisateur de se connecter au site et d'y afficher des flux RSS et Atom qu'il a préalablement ajouté.


D'abord l'utilisateur doit se créer un compte, puis est redirigé sur la page de connexion où il entre ses identifiants.
Ensuite, il ajoute les flux RSS/Atom qu'il souhaite suivre.
Pour visualiser les flux suivis, il se rend dans l'onglet « Flux ».
D'ici, l'utilisateur voit les flux ajoutés, il lui suffit de cliquer dessus pour voir en détail les actualités, mais il peut aussi supprimer les flux qu'il ne souhaite plus suivre.
Quand il a finit d'utiliser l'application, l'utilisateur peut se déconnecter.


Un utlisateur nommé test@gmail.com avec comme identifiant test et comme mot de passe test est déjà présent dans la base, il est déjà abonné à deux flux.